<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ParticipateInForumTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */

    /** @test */
    public function an_authenticated_user_can_participate_in_forum(){
        $this->signIn();
        $thread = create('App\Thread');
        $reply = make('App\Reply');
        $this->post($thread->path().'/replies', $reply->toArray());

        $this->assertDatabaseHas('replies',['body' => $reply->body]);
        $this->assertEquals(1,$thread->fresh()->replies_count);
    }
    /** @test */
    public function a_reply_requires_a_body(){
        $this->withExceptionHandling()->signIn();
        $thread = create('App\Thread');
        $reply = make('App\Reply',['body' => null]);
        $this->json('post',$thread->path().'/replies', $reply->toArray())
            ->assertStatus(422);
    }
    /** @test */
    public function unauthorized_users_cannot_delete_reply(){
        $this->withExceptionHandling();
        $reply = create('App\Reply');

        $this->delete("/replies/{$reply->id}")
            ->assertRedirect('login');
        $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);
    }
    /** @test */
    public function authorized_users_can_delete_replies(){
        $this->signIn();
        $reply = create('App\Reply',['user_id' => auth()->id()]);
            $this->delete("/replies/{$reply->id}")->assertStatus(302);
               $this->assertDatabaseMissing('replies',['id' => $reply->id]);
        $this->assertEquals(0,$reply->thread->fresh()->replies_count);
    }
    /** @test */
    public function authorized_users_can_update_replies(){
        $this->signIn();
        $updatedReply = 'you been changed fool' ;
        $reply = create('App\Reply',['user_id' => auth()->id()]);
        $this->patch("/replies/{$reply->id}",['body' => $updatedReply]);
        $this->assertDatabaseHas('replies',['id' => $reply->id, 'body' => $updatedReply ]);
    }
    /** @test */
    public function unauthorized_users_cannot_update_reply(){
        $this->withExceptionHandling();
        $reply = create('App\Reply');

        $this->patch("/replies/{$reply->id}")
            ->assertRedirect('login');
        $this->signIn()
            ->patch("/replies/{$reply->id}")
            ->assertStatus(403);
    }
    /** @test */
    public function replies_that_contain_spam_may_not_be_created(){
        $this->withExceptionHandling();
        $this->signIn();
        $thread = create('App\Thread');
        $reply = make('App\Reply',
            [
                'body' => 'Yahoo customer support'
            ]);

        $this->json('post',$thread->path().'/replies', $reply->toArray())
            ->assertStatus(422);
    }

    /** @test */
    function users_may_only_reply_once_per_minute(){
        $this->withExceptionHandling();
        $this->signIn();
        $thread = create('App\Thread');
        $reply = make('App\Reply',
            [
                'body' => 'My simply reply'
            ]);
        $this->post($thread->path().'/replies', $reply->toArray())
            ->assertStatus(200);
        $this->post($thread->path().'/replies', $reply->toArray())
            ->assertStatus(429);
    }
}

