<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;


class ThreadsTest extends TestCase
{
    use DatabaseMigrations;
    protected $thread;
    /**
     * A basic test example.
     *
     * @return void
     */

    public function setUp()
    {
        parent::setUp(); // TODO: Change the autogenerated stub
        $this->thread = create('App\Thread');
    }
    /** @test */
    public function a_user_can_browse_threads()
    {
        $this->get('/threads')
        ->assertSee($this->thread->title);
    }
    /** @test */
    public function a_thread_has_a_path(){

        $this->assertEquals( '/threads/'.$this->thread->channel->slug.'/'.$this->thread->slug,$this->thread->path());
    }
    /** @test */
    public function a_user_can_view_single_thread(){
       $this->get($this->thread->path())
        ->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_filter_threads_according_to_a_tag(){
        $channel = create('App\Channel');
        $threadInChannel = create('App\Thread',['channel_id' => $channel->id]);
        $threadNotInChannel = create('App\Thread');
        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }
    /** @test */
    public function a_user_can_filter_threads_by_username(){
        $this->signIn(create('App\User',['name' => 'Bradley Yarrow']));

        $threadByBradley = create('App\Thread',['user_id' => auth()->id()]);
        $threadNotByBradley = create('App\Thread');

        $this->get('threads?by=Bradley Yarrow')
            ->assertSee($threadByBradley->title)
            ->assertDontSee($threadNotByBradley->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_popularity(){
        //Given we have three threads
        $threadWithTwoReplies = create('App\Thread');
        create('App\Reply',['thread_id' => $threadWithTwoReplies->id],2);
        $threadWithThreeReplies = create('App\Thread');
        create('App\Reply',['thread_id' => $threadWithThreeReplies->id],3);

        $threadWithNoReplies = $this->thread;
        //With 2 replies, 3 replies or 0 replies
        //When i filter all threads by popularity
        $response = $this->getJson('threads?popular=1')->json();
        $this->assertEquals([3,2,0],array_column($response['data'],'replies_count'));
        //Then they should be returned from most reply to least
    }
    /** @test */
    public function a_user_can_filter_thread_by_those_that_are_unanswered(){
        $thread = create('App\Thread');
        create('App\Reply',['thread_id' => $thread->id]);
        $response = $this->getJson('threads?unanswered=1')->json();
        $this->assertCount(1,$response['data']);
    }

    /** @test */
    public function a_user_can_request_all_replies_for_given_thread(){
        $thread = create('App\Thread');
        create('App\Reply',['thread_id' => $thread->id]);
        $response = $this->getJson($thread->path().'/replies')->json();
        $this->assertCount(1,$response['data']);
        $this->assertEquals(1,$response['total']);
    }
    /** @test */
    function we_record_a_new_visit_each_time_the_thread_is_read()
    {
        $thread = create('App\Thread');

        $this->assertSame(0, $thread->visits);

        $this->call('GET', $thread->path());

        $this->assertEquals(1, $thread->fresh()->visits);
     }
}
