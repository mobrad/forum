<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Notifications\DatabaseNotification;
use Tests\TestCase;


class NotificationsTest extends TestCase
{
    use DatabaseMigrations;
    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test */


    public function a_notification_is_prepared_when_a_subscribed_thread_recieves_a_new_reply_that_is_not_by_me()
    {
        $this->signIn();
        $thread = create('App\Thread')->subscribe();
        $this->assertCount(0,auth()->user()->notifications);
        // Then each time a new reply is left
        $thread->addReply([
            'user_id' => auth()->id(),
            'body' => 'some reply here'
        ]);

        //Then a notification should be prepared for the user
        $this->assertCount(0,auth()->user()->fresh()->notifications);
        $thread->addReply([
            'user_id' => create('App\User')->id,
            'body' => 'some reply here'
        ]);
        $this->assertCount(1,auth()->user()->fresh()->notifications);
    }

    /** @test */
    public function a_user_can_mark_a_notification_as_read(){
        $this->signIn();
        create(DatabaseNotification::class);
        tap(auth()->user(),function($user){
            $this->assertCount(1,$user->unreadNotifications);
            $this->delete("/profiles/{$user->name}/notifications/". auth()->user()->unreadNotifications->first()->id);
            $this->assertCount(0,$user->fresh()->unreadNotifications);
        });


    }

    /** @test */
    public function a_user_can_fetch_unread_notifications(){
        $this->signIn();
        create(DatabaseNotification::class);
        $this->assertCount(1, $this->getJson("/profiles/".auth()->user()->name."/notifications")->json());
    }
}
