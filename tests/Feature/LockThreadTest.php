<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;




class LockThreadTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    function non_administrators_may_not_lock_thread(){
        $this->withExceptionHandling();
        $this->signIn();
        $thread = create('App\Thread',['user_id' => auth()->id()]);
        $this->post(route('lock-threads.store',$thread));
        $this->assertFalse(!! $thread->fresh()->locked);
    }
    /** @test */
    function administrators_can_lock_threads(){
        $this->withExceptionHandling();
        $this->signIn(factory('App\User')->states('administrator')->create());
        $thread = create('App\Thread',['user_id' => auth()->id()]);
        $this->post(route('lock-threads.store',$thread));
        $this->assertTrue(!! $thread->fresh()->locked,'Fails her');

    }
    /** @test */
    function administrators_can_unlock_threads(){
        $this->withExceptionHandling();
        $this->signIn(factory('App\User')->states('administrator')->create());
        $thread = create('App\Thread',['user_id' => auth()->id(),'locked' => false]);
        $this->delete(route('lock-threads.destroy',$thread));
        $this->assertFalse(!! $thread->fresh()->locked,'Fails her');

    }
    /** @test */
    public function once_locked_a_thread_may_not_recieve_replies()
    {
        $this->signIn();
        $thread = create('App\Thread',['locked' => true]);
        $this->post($thread->path().'/replies',[
            'body' => 'Foobar',
            'user_id' => auth()->id()
        ])->assertStatus(422);
    }
}
