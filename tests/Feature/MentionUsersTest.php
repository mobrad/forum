<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;




class MentionUsersTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function mentioned_users_in_a_reply_are_notified()
    {
        //Given i have a signed in user
        $john = create('App\User',['name' => 'JohnDoe']);
        $this->signIn($john);
        //And another user
        $jane = create('App\User',['name' => 'JaneDoe']);
        //If we have a thread
        $thread = create('App\Thread');
        //And johndoe replies and Mentions Janedoe
        $reply = create('App\Reply',[
           'body' => '@JaneDoe look at this'
        ]);
        //Then janeDoe should be notified
        $this->json('post',$thread->path().'/replies', $reply->toArray());
        $this->assertCount(1,$jane->notifications);
    }
    /** @test */
    function it_can_fetch_all_mentioned_users_starting_with_the_givem_character(){
        create('App\User',['name' => 'johndoe']);
        create('App\User',['name' => 'johndoe2']);
        create('App\User',['name' => 'janedoe']);
        $results = $this->json('GET','/api/users',['name' => 'john']);

        $this->assertCount(2,$results->json());
    }
}
