<?php

namespace Tests\Feature;

use App\Mail\PleaseConfirmYourEmail;
use App\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;




class RegisterationTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function a_confirmation_email_is_sent_upon_registeration()
    {
        Mail::fake();
       event(new Registered(create('App\User')));
        //dd($fish);
        Mail::assertSent(PleaseConfirmYourEmail::class);
    }
    /** @test */
    public function user_can_fully_confirm_their_email(){
        Mail::fake();
        $this->post(route('register'),[
            'name' =>'Bradley',
            'email' => 'bradley@example.com',
            'password' => 'foobar',
            'password_confirmation' => 'foobar'
        ]);
        $user = User::whereName('Bradley')->first();
        $this->assertFalse($user->confirmed);
        $this->assertNotNull($user->confirmation_token);

        $this->get(route('register.confirm',['token' => $user->confirmation_token]))
            ->assertRedirect(route('threads'));
        tap($user->fresh(),function($user){
            $this->assertTrue($user->confirmed);
            $this->assertNull($user->confirmation_token);
        });


    }
    /** @test */
   public function confirming_an_invalid_token(){
       $this->get(route('register.confirm',['token' => 'invalid']))
       ->assertRedirect(route('threads'))
       ->assertSessionHas('flash','invalid token');
   }
}
