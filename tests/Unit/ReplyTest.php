<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReplyTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    /** @test  */
    public function reply_has_owner()
    {
        $reply = factory('App\Reply')->create();

        $this->assertInstanceOf('App\User',$reply->owner);
    }
    /** @test */
    public function it_knows_if_it_was_just_published(){
        $reply = create('App\Reply');
        $this->assertTrue($reply->wasJustPublished());
        $reply->created_at = Carbon::now()->subMonth();
        $this->assertFalse($reply->wasJustPublished());
    }
    /** @test */
    public function it_can_detect_all_metioned_users(){
        $reply = create('App\Reply',[
            'body' => '@JaneDoe wants to talk to @JohnDoe'
        ]);

        $this->assertEquals(['JaneDoe','JohnDoe'],$reply->mentionedUsers());
    }
    /** @test */
    public function it_wraps_all_mentioned_users_within_anchor_tags(){
        $reply = create('App\Reply',[
            'body' => 'Hello @JohnDoe.'
        ]);
        $this->assertEquals(
            'Hello <a href="/profiles/JohnDoe">@JohnDoe</a>.',
            $reply->body
        );
    }
    /** @test */
    public function it_knows_if_its_the_best_reply(){
        $reply = create('App\Reply');
        $this->assertFalse($reply->isBest());
        $reply->thread->update(['best_replies_id' => $reply->id]);
        $this->assertTrue($reply->fresh()->isBest());
    }
    /** @test */
    function a_replies_body_is_sanitized_automatically(){
        $reply = make('App\Reply',['body' => '<script>alert("Bad")</script><p>This is okay</p>']);
        $this->assertEquals("<p>This is okay</p>",$reply->body);
    }
}
