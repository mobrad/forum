<?php

namespace Tests\Feature;

use App\Inspection\Spam;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;




class SpamTest extends TestCase
{
    use DatabaseMigrations;
    /** @test */
    public function it_checks_for_invalid_keywords()
    {
        $spam = new Spam();

        $this->assertFalse($spam->detect("Innocent Reply"));

        $this->expectException('Exception');
        $spam->detect("Yahoo customer support");
    }
    /** @test */
    public function it_test_for_key_held_down(){
        $spam = new Spam();

        $this->expectException('Exception');
        $spam->detect('Hello world aaaaa');
    }
}
