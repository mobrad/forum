<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::view('scan','scan');
Route::get('/', function () {
    return view('welcome');
});
Route::get('/threads','ThreadsController@index')->name('threads');
Route::get('/threads/create','ThreadsController@create')->name('threads.create');
Route::get('/threads/search','SearchController@show')->name('thread.search');
Route::get('/threads/{channel}','ThreadsController@index');
Route::get('/threads/{channel}/{thread}','ThreadsController@show')->name('thread.show');
Route::delete('/threads/{channel}/{thread}','ThreadsController@destroy')->name('thread.delete');
Route::post('/threads/{channel}/{thread}/replies','RepliesController@store')->name('thread.reply');
Route::get('/threads/{channel}/{thread}/replies','RepliesController@index');
Route::patch('/threads/{channel}/{thread}','ThreadsController@update')->name('thread.patch');



Route::post('replies/{reply}/favorites','FavoritesController@store');


Route::post('lock-threads/{thread}','LockThreadsController@store')->name('lock-threads.store')->middleware('admin');
Route::delete('lock-threads/{thread}','LockThreadsController@destroy')->name('lock-threads.destroy')->middleware('admin');
Route::delete('replies/{reply}/favorites','FavoritesController@destroy');
Route::patch('replies/{reply}','RepliesController@update')->name('reply.update');
Route::delete('replies/{reply}','RepliesController@destroy')->name('reply.delete');
Route::post('replies/{reply}/best','BestRepliesController@store')->name('best-replies.store');
Route::post('/threads','ThreadsController@store')->name('threads.store')->middleware('must-be-confirmed');
Route::post('/threads/{channel}/{thread}/subscriptions','ThreadSubscriptionsController@store')->middleware('auth')->name('thread.subscribe');
Route::delete('/threads/{channel}/{thread}/subscriptions','ThreadSubscriptionsController@destroy')->middleware('auth')->name('thread.unsubscribe');
//Route::resource('threads','ThreadsController');
Auth::routes();

Route::get('/profiles/{user}','ProfileController@show')->name('profile');
Route::delete('/profiles/{user}/notifications/{notification}','UserNotificationsController@destroy')->name('profile.notification');
Route::get('/profiles/{user}/notifications','UserNotificationsController@index');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register/confirm','Auth\RegistrationConfirmationController@index')->name('register.confirm');

Route::get('api/users','Api\UsersController@index');
Route::post('api/users/{user}/avatar','Api\UserAvatarController@store')->name('profile.avatar');