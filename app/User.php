<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','avatar_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','hidden',
    ];
    protected $casts = [
      'confirmed' => 'boolean'
    ];
    public function getRouteKeyName()
    {
        return 'name';
    }
    public function threads(){
        return $this->hasMany(Thread::class)->latest();
    }
    public function lastReply(){
        return $this->hasOne(Reply::class)->latest();
    }
    public function activity(){
        return $this->hasMany(Activity::class);
    }
    public function read($thread){
        cache()->forever($this->visitedThreadCacheKey($thread),Carbon::now());
    }
    public function confirm(){
        $this->confirmed = true ;
        $this->confirmation_token = null;
        $this->save();
    }
    public function visitedThreadCacheKey($thread){
        return sprintf("user.%s.visits.%s",$this->id,$thread->id);

    }
    public function getAvatarPathAttribute($avatar){
        if (!$avatar){
            return '/images/avatar/avatar.svg';
        }
        return '/storage/'.$avatar;
    }
    public function isAdmin(){
        return in_array($this->name,['Bradley Yarrow','JohnDoe']);
    }
}
