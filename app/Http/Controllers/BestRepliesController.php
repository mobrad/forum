<?php

namespace App\Http\Controllers;

use App\Reply;
use Illuminate\Http\Request;

class BestRepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Reply $reply){
        $this->authorize('update',$reply->thread);

        $reply->thread->markBestReply($reply);
    }
}
