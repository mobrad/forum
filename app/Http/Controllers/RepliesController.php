<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePostRequest;
use App\Notifications\YouWereMentioned;
use App\Reply;
use App\Thread;
use App\User;


class RepliesController extends Controller
{

    /**
     * RepliesController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth',['except' => 'index']);
    }

    public function index($channeld,Thread $thread){
        return $thread->replies()->paginate(20);
    }

    public function store($channeld, Thread $thread, CreatePostRequest $form){
        if($thread->locked){
            return response('Thread is locked',422);
        }
        return $thread->addReply([
                'body' => request('body'),
                'user_id' => auth()->id(),
        ])->load('owner');

    }

    public function destroy(Reply $reply){
        $this->authorize('update',$reply);
        $reply->delete();
        if (request()->expectsJson()){
            return response(['status' => 'Reply deleted']);
        }
        return back();
    }
    public function update(Reply $reply){
        $this->authorize('update',$reply);
            request()->validate(["body" => "required|spamfree"]);
            $reply->update(request(['body']));

    }


}
