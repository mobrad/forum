<?php

namespace App\Http\Controllers;

use App\Channel;
use App\Rules\Recaptcha;
use App\Thread;
use App\Filters\ThreadFilters;
use App\Trending;
use Illuminate\Http\Request;

class ThreadsController extends Controller
{
    /**
     * ThreadsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @param Trending $trending
     * @return \Illuminate\Http\Response
     * @internal param null $channelSlug
     * @internal param null $channelId
     */
    public function index(Channel $channel,ThreadFilters $filters, Trending $trending)
    {
        $threads = $this->getThreads($channel, $filters);

        if (request()->wantsJson()){
            return $threads;

        }
        ;
        return view('threads.index',[
            'threads' => $threads,
            'trending' => $trending->get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('threads.create');
    }

    /**
     * @param Recaptcha $recaptcha
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Recaptcha $recaptcha)
    {

        request()->validate([
            "title" => "required|spamfree",
            "body" => "required|spamfree",
            "channel_id" => "required|exists:channels,id",
            'g-recaptcha-response' => [ $recaptcha ]
        ]);

        $thread = Thread::create([
            'user_id' => auth()->id(),
            'title' => request('title'),
            'body' =>  request('body'),
            'channel_id' => request('channel_id')

        ]);
        if (request()->wantsJson()){
            return response($thread,201);
        }
       return redirect($thread->path())->with('flash','Thread created');
    }

    /**
     * Display the specified resource.
     *
     * @param $channelId
     * @param  \App\Thread $thread
     * @param Trending $trending
     * @return \Illuminate\Http\Response
     */
    public function show($channelId, Thread $thread, Trending $trending)
    {
        if (auth()->check()){
            auth()->user()->read($thread);
        }
        $trending->push($thread);
        $thread->increment('visits');

        return view('threads.show',compact('thread'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function edit(Thread $thread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update($channel, Thread $thread)
    {
        //
        $this->authorize('update',$thread);

        $thread->update(request()->validate(['title' => 'required|spamfree','body' => 'required|spamfree']));
        return $thread;
    }

    /**
     * Remove the specified resource from storage.
     * @param $channelId
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function destroy($channelId,Thread $thread)
    {
        $this->authorize('update',$thread);

        $thread->delete();
        if (request()->wantsJson()){
            return response([],204);
        }
        return redirect("/threads");
    }

    /**
     * @param Channel $channel
     * @param ThreadFilters $filters
     * @return mixed
     */
    public function getThreads(Channel $channel, ThreadFilters $filters)
    {
        $threads = Thread::with('channel')->latest()->filter($filters);
        if ($channel->exists) {
            $threads->where('channel_id', $channel->id);
        }
        $threads = $threads->paginate(25);
        return $threads;
    }


    /**
     * @param Channel $channel
     * @return \Illuminate\Database\Query\Builder|\Illuminate\Support\Collection|static
     */

}
