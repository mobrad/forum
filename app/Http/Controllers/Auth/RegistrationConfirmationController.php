<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;

class RegistrationConfirmationController extends Controller
{
    public function index(){

           $user = User::where('confirmation_token',request('token'))->first();
           if(! $user) {
               return redirect(route('threads'))->with('flash','invalid token');
           }
            $user->confirm();
        return redirect('/threads')->with('flash','Your account has been confirmed');
    }
}
