<?php
/**
 * Created by PhpStorm.
 * User: Tammy
 * Date: 21/01/2018
 * Time: 15:33
 */

namespace App\Inspection;
use Exception;

class KeyHeldDown
{

    public function detect($body){
        if (preg_match('/(.)\\1{4,}/',$body)){
            throw new Exception('Your reply contains spam');
        }
    }
}