<?php
/**
 * Created by PhpStorm.
 * User: Tammy
 * Date: 21/01/2018
 * Time: 15:26
 */

namespace App\Inspection;
use Exception;

class InvalidKeywords
{
    protected $keywords = [
        'yahoo customer support'
    ];
    public function detect($body){


        foreach ($this->keywords as $keyword){
            if (stripos($body,$keyword) !== false){
                throw new Exception('Your reply contains spam');
            }
        }
    }

}