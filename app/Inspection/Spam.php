<?php
/**
 * Created by PhpStorm.
 * User: Tammy
 * Date: 21/01/2018
 * Time: 14:57
 */

namespace App\Inspection;


class Spam
{
    protected $inspections = [
        InvalidKeywords::class,
        KeyHeldDown::class
    ];
    public function detect($body){

        foreach ($this->inspections as $inspection){
            app($inspection)->detect($body);
        }
        return false;
    }


}