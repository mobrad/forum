<?php
/**
 * Created by PhpStorm.
 * User: Tammy
 * Date: 28/12/2017
 * Time: 16:11
 */
namespace App\Filters;
use App\User;
use Illuminate\Http\Request;

class ThreadFilters extends Filters {

    protected $filters = ['by','popular','unanswered'];
    /**
     * Filter the query by a given username
     * @param $username
     * @return mixed
     * @internal param $builder
     */
    public function by($username)
    {
        
        $user = User::where('name', $username)->firstOrFail();
        return $this->builder->where('user_id', $user->id);
    }

    /**
     * Filter the query by most popular threads
     * @return mixed
     */
    public function popular(){
        $this->builder->getQuery()->orders = [];
        return $this->builder->orderBy('replies_count','desc');
    }

    public function unanswered(){
        return $this->builder->where('replies_count',0);
    }
}