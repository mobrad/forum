<?php
/**
 * Created by PhpStorm.
 * User: Tammy
 * Date: 12/01/2018
 * Time: 12:12
 */

namespace App;


trait RecordsActivity
{
    //boots this into whatever uses the trait
    protected static function bootRecordsActivity(){
        if (auth()->guest())return;

        foreach (static::getActivitiesToRecord() as $event){
            static::$event(function ($model) use ($event){
                $model->recordActivity($event);
            });

        }
        static::deleting(function($model){
            $model->activity()->delete();
        });
    }
    protected static function getActivitiesToRecord(){
        return ['created'];
    }
    protected function recordActivity($event)
    {
       $this->activity()->create([
            'type' => $this->getActivityType($event),
            'user_id' => auth()->id(),
        ]);
    }

    public function activity(){
        return $this->morphMany('App\Activity','subject');
    }

    protected function getActivityType($event)
    {
        $type = strtolower((new \ReflectionClass($this))->getShortName());
        return "{$event}_{$type}";
    }
}