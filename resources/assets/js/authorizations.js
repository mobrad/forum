/**
 * Created by Bradley on 09/02/2018.
 */
let  user = window.App.user;
module.exports = {

    owns(model,prop = "user_id"){
        return model[prop] === user.id
    },
    isAdmin(){
        return ['Bradley Yarrow'].includes(user.name)
    }
};