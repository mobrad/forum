@extends('layouts.app')
@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
    @endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">

                    <form action="{{route('threads.store')}}" method="POST">
                        {{csrf_field()}}
                        <h3>Create a new Thread</h3>
                        <div class="form-group">
                            <label for="channel">Channel</label>
                            <select name="channel_id" id="channel" class="form-control" required>
                                <option value="">Select a channel</option>
                                @foreach($channels as $channel)
                                    <option value="{{$channel->id}}" {{old('channel_id') == $channel->id ? 'selected' : ''}} >{{$channel->name}}</option>
                                    @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="title">Title</label>
                            <input type="text" name="title" id="title" class="form-control" placeholder="Enter title of the thread" value="{{old('title')}}" required>
                        </div>
                        <div class="form-group">
                            <label for="content">Thread Content</label>
                            <wysiwyg></wysiwyg>
                        </div>
                        <div class="g-recaptcha" data-sitekey="6Lfp5UUUAAAAAD5vfxUF78AVfZtbJ5w1FaRwUPPR"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Publish</button>
                        </div>
                        @if(count($errors))
                                <ul class="alert alert-danger">
                                    @foreach($errors->all() as $error)
                                        <li>{{$error}}</li>
                                        @endforeach
                                </ul>
                            @endif
                    </form>

            </div>
        </div>
    </div>
@endsection
